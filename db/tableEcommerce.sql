-- si disegnano prima i db che hanno realzione one, e dopo quelli con relazione many
USE crud_ecommerce;
-- Table structure for table `users`
USE crud_ecommerce;
CREATE TABLE IF NOT EXISTS `users` (
  `user_id` int(11) NOT NULL AUTO_INCREMENT,
  `user_email` varchar(500) COLLATE latin1_german2_ci DEFAULT NULL,
  `user_password` varchar(500) COLLATE latin1_german2_ci DEFAULT NULL,
  `user_firstName` varchar(50) COLLATE latin1_german2_ci DEFAULT NULL,
  `user_lastName` varchar(50) COLLATE latin1_german2_ci DEFAULT NULL,
  PRIMARY KEY (`user_id`)
) --
-- Table structure for table `orders`
-- -- relazione many to one (abbiamo solo un fk)
CREATE TABLE IF NOT EXISTS `orders` (
  `order_id` int(11) NOT NULL AUTO_INCREMENT,
  `order_user_id` int(11) NOT NULL,
  `order_amount` float NOT NULL,
  `order_phone` varchar(20) COLLATE latin1_german2_ci NOT NULL,
  `order_email` varchar(100) COLLATE latin1_german2_ci NOT NULL,
  PRIMARY KEY (`order_id`),
  CONSTRAINT fk_orders_users -- è collegato all'id di user
  FOREIGN KEY (order_user_id) REFERENCES users(user_id) ON DELETE CASCADE ON UPDATE CASCADE
) --
-- Table structure for table `productcategories`
USE crud_ecommerce;
CREATE TABLE IF NOT EXISTS `productcategories` (
  `category_id` int(11) NOT NULL AUTO_INCREMENT,
  `category_name` varchar(50) COLLATE latin1_german2_ci NOT NULL,
  PRIMARY KEY (`category_id`)
) --
-- Dumping data for table `products`
-- relazione many to one
USE crud_ecommerce;
CREATE TABLE IF NOT EXISTS `products` (
  -- viene collegato da productOptions
  `product_id` int(12) NOT NULL AUTO_INCREMENT,
  `product_name` varchar(100) COLLATE latin1_german2_ci NOT NULL,
  `product_price` float NOT NULL,
  `product_image` varchar(100) COLLATE latin1_german2_ci NOT NULL,
  -- è collegato all'id di product category
  `product_category_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`product_id`),
  CONSTRAINT fk_products_productcategories FOREIGN KEY (product_category_id) REFERENCES productcategories(category_id) ON DELETE CASCADE ON UPDATE CASCADE
) --
-- Table structure for table `orderdetails`
-- relazione many to one (abbiamo due fk)
USE crud_ecommerce;
CREATE TABLE IF NOT EXISTS `orderdetails` (
  `detail_id` int(11) NOT NULL AUTO_INCREMENT,
  -- è collegato all'id di order
  `detail_order_id` int(11) NOT NULL,
  -- è collegato all'id di products
  `detail_product_id` int(11) NOT NULL,
  `detail_name` varchar(250) COLLATE latin1_german2_ci NOT NULL,
  `detail_price` float NOT NULL,
  `detail_quantity` int(11) NOT NULL,
  PRIMARY KEY (`detail_id`),
  CONSTRAINT fk_orderdetails_orders FOREIGN KEY (detail_order_id) REFERENCES orders(order_id) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT fk_orderdetails_products FOREIGN KEY (detail_product_id) REFERENCES products(product_id) ON DELETE CASCADE ON UPDATE CASCADE
)