Progetto di Ecommerce

## Descrizione

Il progetto ecommerce userà le seguente tecnologie:

- **`Backend: Spring`**
- **`DB: SQL`**
- **`Frontend: Angular`**

## Prerequisiti

- `Backend: Eclipse`
- `DBMS: Software MySQL`
- `Frontend: Visual Studio Code`

### Passi da eseguire

- _git init_: per iniziare git sulla tua cartella di lavoro
- _git clone https://gitlab.com/fabian87/ecommerceproject.git_: per scaricare il progetto
- _cd eccomerceproject_: ti sposti sulla cartella che hai appena scaricato
- _git chekout -b develop_: Creazione di un branch figlio di master e ti sposta sul branch develop
- _git chekout -b feature/nomeUser_: Creazione di un branch figlio di develop e ti sposta sul branch feature/nomeUser
- _git add ._: aggiungere tutte le tue modifiche
- _git commit -m "Descrizione della tua modifica."_
- _git push -u origin feature/nomeUser_: per aggiornare il repository da locale a remoto (su GitLab). "-u origin feature/nomeUser", viene usato la prima volta, dopo basterà solo "git push"
- _git pull -u origin origin/develop_: per scaricarti l'ultimo aggiornamento del branch develop

#### descrizione della creazione del branch

_In un flusso, si deve sempre lavorare su un branch figlio di develop che di solito viene chiamato:
feature/nome release/nome o hotfix/nome . develop è usato solo ed esclusivamente per la produzione quando tutto è già pronto_
